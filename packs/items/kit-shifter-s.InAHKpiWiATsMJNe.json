{
  "_id": "InAHKpiWiATsMJNe",
  "name": "Kit, Shifter's",
  "type": "container",
  "img": "icons/containers/chest/chest-reinforced-steel-red.webp",
  "system": {
    "inventoryItems": [
      {
        "_id": "lSCPUK5Ea6R0t4fz",
        "name": "Backpack",
        "type": "container",
        "flags": {
          "core": {
            "sourceId": "Item.CcUuzTh1CCp2NMRv"
          }
        },
        "img": "systems/pf1/icons/items/inventory/backpack.jpg",
        "system": {
          "description": {
            "value": "<p>This leather knapsack has one large pocket that closes with a buckled strap and holds about 2 cubic feet of material. Some may have one or more smaller pockets on the sides.</p>"
          },
          "weight": {
            "value": 2
          },
          "price": 2
        }
      },
      {
        "_id": "ygmrcisvftnvjyob",
        "name": "Flint And Steel",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.ygmrcisvftnvjyob"
          }
        },
        "img": "icons/commodities/stone/rock-chunk-grey.webp",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<p>Lighting a <em>torch</em> with a <em>flint and steel</em> is a full-round action. Lighting any other fire with them takes at least that long.</p>"
          },
          "price": 1,
          "equipped": false
        }
      },
      {
        "_id": "snfogneawzopfzzl",
        "name": "Torch",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.snfogneawzopfzzl"
          }
        },
        "img": "systems/pf1/icons/items/inventory/torch.jpg",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<p><b>Price</b> 1 cp; <b>Weight</b> 1 lb.</p>\n<p>A torch burns for 1 hour, shedding normal light in a 20-foot radius and increasing the light level by one step for an additional 20 feet beyond that area (darkness becomes dim light and dim light becomes normal light). A torch does not increase the light level in normal light or bright light. If a torch is used in combat, treat it as a one-handed improvised weapon that deals bludgeoning damage equal to that of a gauntlet of its size, plus 1 point of fire damage.</p>"
          },
          "quantity": 10,
          "weight": {
            "value": 1
          },
          "price": 0.01,
          "equipped": false
        }
      },
      {
        "_id": "bsztkhkrhgmsbqpt",
        "name": "Waterskin",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.bsztkhkrhgmsbqpt"
          }
        },
        "img": "systems/pf1/icons/items/inventory/waterskin.jpg",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>A water or wineskin holds 1/2 gallon of liquid and weighs 4 lb when full.</p>\n<p><b>Empty Weight</b>: -; <b>Capacity</b>: 1/2 gallon/4 lb.<sup>1</sup></p>\n<p class=\"source\"><sup>1</sup> When made for Medium characters. Weighs one-quarter the normal amount when made for Small characters. Weighs twice the normal amount when made for Large characters. Containers carry one-quarter the normal amount when made for Small characters.</p>\n<p></p>\n<h3>Magic Containers</h3>\n<p>Magic containers come in almost as many varieties as there are <em>alchemists</em>, and so are of inestimable value to <em>alchemists</em> with specific needs. Some of the most well-known magic containers are described here.</p>\n<p>Because of a varied nature of alchemical items and their effects, not all combinations of alchemical items and magic bottles are viable or even make sense, even though they technically might be allowed by the rules. The GM should have the final say on whether or not a particular alchemical item can function within one of the magical containers listed in this section.</p>\n<table border=\"1\" cellpadding=\"5\">\n<thead>\n<tr>\n<th colspan=\"4\">Magic Containers</th>\n</tr>\n<tr>\n<th>Item</th>\n<th>Price</th>\n<th>Weight</th>\n<th>Source</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td><i>Focusing Flask</i></td>\n<td>700 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Retort of Control</i></td>\n<td>13,000 gp</td>\n<td class=\"marker\">-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Vial of Efficacious Medicine</i></td>\n<td>7,000 gp</td>\n<td class=\"marker\">-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Winged Bottle</i></td>\n<td>1,620 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n</tbody>\n</table>"
          },
          "weight": {
            "value": 4
          },
          "price": 1,
          "equipped": false
        }
      },
      {
        "_id": "manvtbznrwjmnrua",
        "name": "Rations, Trail",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.manvtbznrwjmnrua"
          }
        },
        "img": "systems/pf1/icons/items/inventory/meat.jpg",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<h4>Rations, Trail</h4>\n<p><b>Price</b> 5 sp; <b>Weight</b> 1 lb.</p>\n<p>The listed price is for a day's worth of food. This bland food is usually some kind of hard tack, jerky, and dried fruit, though the contents vary from region to region and the race of those creating it. As long as it stays dry, it can go for months without spoiling.</p>"
          },
          "quantity": 5,
          "weight": {
            "value": 1
          },
          "price": 0.5,
          "equipped": false
        }
      },
      {
        "_id": "qlmtnppwxkubqyhp",
        "name": "Rope",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.qlmtnppwxkubqyhp"
          }
        },
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<h4>Rope</h4>\n<p><b>Hempen</b> The DC to escape hemp rope bonds is equal to 20 + the CMB of the creature that tied the bonds. Ropes do not need to make a check every round to maintain the pin. If the DC to escape is higher than 20 + the tying creatures CMB, the tied up creature cannot escape from the bonds, even with a natural 20 on the check. This rope has 2 hit points and can be burst with a DC 23 Strength check. <b>Price</b> 1 gp; <b>Weight</b> 10 lbs.</p>\n<p><b>FYI</b>: The <em>Equipment Trick</em> feat provides a number of options for using this item in combat.</p>\n<p><b>Silk Rope</b> This 50-foot length of silk rope has 4 hit points and can be broken with a DC 24 Strength check. <b>Price</b> 10 gp; <b>Weight</b> 5 lbs.</p>\n<p><b>Bloodvine Rope</b> This 50-foot length of tough, lightweight rope is made from alchemically treated bloodvine, a rare scarlet-colored vine that grows only in warm jungle environments. Though prized by climbers for its durability, bloodvine can also be used to bind creatures. Bloodvine rope has a <em>hardness</em> of 5 and 10 <em>hit points</em>, and can be broken with a DC 30 <em>Strength</em> check. A creature bound by bloodvine rope can escape with a DC 35 <em>Escape Artist</em> check or a DC 30 <em>Strength</em> check. The DC to create bloodvine rope with <em>Craft</em> (alchemy) is 30. <b>Price</b> 200 gp; <b>Weight</b> 5 lbs. <b>Source</b> <em>PRG:ACG</em></p>\n<p><b>Spider's Silk Rope</b> This 50-foot length of rope is woven of strands of silk from monstrous spiders. Rare to virtually nonexistent on the surface world, it is commonly used by the dark elves, though shorter spider's silk rope scraps (generally no more than 10 feet long) occasionally appear among goblins. Spider's silk rope has 6 hit points and can be broken with a DC 25 Strength check. <b>Price</b> 100 gp; <b>Weight</b> 4 lbs.</p>"
          },
          "weight": {
            "value": 10
          },
          "price": 1,
          "equipped": false
        }
      },
      {
        "_id": "habofoapdifirevm",
        "name": "Pot, Cooking (Iron)",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.habofoapdifirevm"
          }
        },
        "img": "icons/tools/cooking/cauldron-empty.webp",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>Cooking pots come in a variety of materials, but the most common is formed of iron.</p>\n<p>A mithral cooking pot weighs 2 lbs. and costs 2,001 gp.</p>\n<p><b>Empty Weight</b>: 2 lb.; <b>Capacity</b>: 1 gallon/8 lb.</p>"
          },
          "weight": {
            "value": 4
          },
          "price": 0.8,
          "equipped": false
        }
      },
      {
        "_id": "Ueae6c2qqJJGpncA",
        "name": "Mess Kit",
        "type": "container",
        "flags": {
          "core": {
            "sourceId": "Item.F1QKvOYGM9u5YI1x"
          }
        },
        "img": "icons/containers/chest/chest-simple-box-red.webp",
        "system": {
          "description": {
            "value": "<p>This kit includes a plate, bowl, cup, fork, knife, and spoon, made of wood, horn, or tin. Each item has a handle or small hole, and can be tied together using the included leather cord.</p>"
          },
          "weight": {
            "value": 0.1
          },
          "price": 0.2,
          "inventoryItems": [
            {
              "name": "Plate",
              "type": "loot",
              "_id": "t95qxxcbkgau4zwa",
              "img": "systems/pf1/icons/items/inventory/dice.jpg",
              "system": {
                "subType": "tradeGoods",
                "weight": {
                  "value": 0.4
                },
                "equipped": false
              }
            },
            {
              "name": "Bowl",
              "type": "loot",
              "img": "icons/containers/kitchenware/bowl-clay-brown.webp",
              "_id": "q4asw26dyrjavece",
              "system": {
                "subType": "tradeGoods",
                "weight": {
                  "value": 0.2
                },
                "equipped": false
              }
            },
            {
              "name": "Fork",
              "type": "loot",
              "img": "icons/tools/cooking/fork-steel-tan.webp",
              "_id": "e1r0sdabl9awmxid",
              "system": {
                "subType": "tradeGoods",
                "weight": {
                  "value": 0.1
                },
                "equipped": false
              }
            },
            {
              "name": "Knife",
              "type": "loot",
              "img": "icons/tools/cooking/knife-chef-steel-brown.webp",
              "_id": "swpiosj6uadvp8xy",
              "system": {
                "subType": "tradeGoods",
                "weight": {
                  "value": 0.1
                },
                "equipped": false
              }
            },
            {
              "name": "Spoon",
              "type": "loot",
              "img": "icons/tools/cooking/soup-ladle.webp",
              "_id": "pxs9209y5ybsemki",
              "system": {
                "subType": "tradeGoods",
                "weight": {
                  "value": 0.1
                },
                "equipped": false
              }
            }
          ]
        }
      },
      {
        "_id": "fwfwshvyweqbthwk",
        "name": "Blanket",
        "type": "loot",
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.fwfwshvyweqbthwk"
          }
        },
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "subType": "gear",
          "description": {
            "value": "<h4>Blanket</h4>\n<p class=\"source\"><b>Source</b> <em>PZO1115</em></p>\n<p><b>Price</b> 5 sp; <b>Weight</b> 3 lbs.</p>\n<p>This warm, woven blanket has straps so it can be rolled up and tied. Blankets are often used in conjunction with bedrolls to provide additional warmth or a ground cushion.</p>\n<p><b>Winter blanket</b> This is a fur blanket large enough for one person. <b>Source</b> <em>PZO1110</em></p>"
          },
          "weight": {
            "value": 3
          },
          "price": 0.5,
          "equipped": false
        }
      }
    ]
  }
}
